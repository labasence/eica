#! /usr/bin/env python
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa EICA
# Copyright 2014-2017 Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://j.mp/GNU_GPL3>`__.
#
# EICA é um software livre; você pode redistribuí-lo e/ou
# modificá-lo dentro dos termos da Licença Pública Geral GNU como
# publicada pela Fundação do Software Livre (FSF); na versão 2 da
# Licença.
#
# Este programa é distribuído na esperança de que possa ser útil,
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
# a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
#
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, veja em <http://www.gnu.org/licenses/>

"""TinyDB database Model.

.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

"""
__author__ = 'carlo'
from tinydb import TinyDB, where
import os
# from tinydb.storages import MemoryStorage
from uuid import uuid1
# DBM = lambda :TinyDB(storage=MemoryStorage)

JSONDB = os.path.dirname(__file__) + '/eica.json'

DBF = lambda: TinyDB(JSONDB)
DRECORD = None


class Pessoa:
    def __init__(self, base=DBF):
        self.banco = base()

    def __setitem__(self, key, value):
        if self.banco.contains(where('key') == key):
            self.banco.update(dict(value=value), where('key') == key)
        else:
            self.banco.insert(dict(key=key, value=value))

    def __getitem__(self, key):
        return self.banco.search(where('key') == key)[0]['value']

    def save(self, value):
        key = str(uuid1())
        self.banco.insert(dict(key=key, value=value))
        return key


class Banco:
    def __init__(self, base=DBF):
        self.banco = base()

    def __setitem__(self, key, value):
        if self.banco.contains(where('key') == key):
            self.banco.update(dict(value=value), where('key') == key)
        else:
            self.banco.insert(dict(key=key, value=value))

    def __getitem__(self, key):
        return self.banco.search(where('key') == key)[0]['value']

    def save(self, value):
        key = str(uuid1())
        self.banco.insert(dict(key=key, value=value))
        return key

    def get(self, key):
        return self[key]

    def getlist(self):
        return [[numreg["key"], numreg["value"]["jogada"][0]["tempo"]] for numreg in self.banco.all() if numreg["value"]["jogada"]]

    def set(self, key, value):
        self[key] = value


def tests():
    from tinydb.storages import MemoryStorage

    b = Banco(lambda: TinyDB(storage=MemoryStorage))
    b[1] = 2
    assert b[1] == 2, "falhou em recuperar b[1]: %s" % str(b[1])
    b[1] = 3
    assert b[1] == 3, "falhou em recuperar b[1]: %s" % str(b[1])
    c = b.save(4)
    assert b[c] == 4, "falhou em recuperar b[1]: %s" % str(b[c])
    r = Banco()
    assert "lele" in r.getlist(),  "falhou em listar o banco: %s" % str( r.getlist())


if __name__ == "__main__":
    tests()
else:
    DRECORD = Banco()
