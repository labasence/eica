#! /usr/bin/env python
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa Torre de Londres, Trem e do Wisconsin
# Copyright 2014-2018 Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://j.mp/GNU_GPL3>`__.
#
# Manobrando o Trem é um software livre; você pode redistribuí-lo e/ou
# modificá-lo dentro dos termos da Licença Pública Geral GNU como
# publicada pela Fundação do Software Livre (FSF); na versão 2 da
# Licença.
#
# Este programa é distribuído na esperança de que possa ser útil,
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
# a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
#
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, veja em <http://www.gnu.org/licenses/>

"""TinyDB database Model.

.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

"""
__author__ = 'carlo'
from json import loads
from urllib.request import urlopen
URL = "http://activufrj.nce.ufrj.br/api/getlist"
URLREG = "http://activufrj.nce.ufrj.br/api/getsession?id="

class Activ:
    def __init__(self):
        dataset = urlopen(URL)
        pyset = loads(dataset.read())
        registros = pyset['applist']
        registros2018 = [numreg for data, numreg in registros if data and "2018" in data]
        print(len(registros2018), registros2018)
        urlreg1 = URLREG + '90235c60e284c8ec5b8fc4107300f398'
        pass
        aluno1 = urlopen(urlreg1)
        pyset = loads(aluno1.read())
        print(len(pyset), pyset["session"])
        jogador = Jogador(**pyset["session"])
        print("jogador", jogador.session, jogador.sexo2, jogador.idade1, jogador.idade2, jogador.ano1, jogador.ano2, jogador.escola, jogador.sexo1, jogador.starttime, jogador.tipoescola, jogador.endtime)

class Jogador:
    def __init__(self, sexo2, idade1, idade2, ano1, ano2, escola, sexo1, starttime, tipoescola, endtime):
        self.session = 0
        self.sexo2 = sexo2
        self.idade1 = idade1
        self.idade2 = idade2
        self.ano1 = ano1
        self.ano2 = ano2
        self.escola = escola
        self.sexo1 = sexo1
        self.starttime = starttime
        self.tipoescola = tipoescola
        self.endtime = endtime


if __name__ == '__main__':
    Activ()