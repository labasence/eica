<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/phaser/2.6.1/phaser.min.js"></script>
    <script src="https://cdn.rawgit.com/brython-dev/brython/3.3.4/www/src/brython.js"></script>
    <script type="text/python">
        from wisconsin.wisconsin import main
        main("{{doc_id}}")
    </script>
    <title>Wisconsin</title>
    <link rel="stylesheet" href="wis/css/wisconsin.css" type="text/css"/>
    <link rel="stylesheet" href="wis/css/style.css" type="text/css"/>

  </head>
  <body onload="brython()">
    <form action="click" method="post">
        <input type="hidden" name="shape" value="" />
        <input type="hidden" name="status" value="" />
        <input type="hidden" name="score" value="" />
        <input type="hidden" name="criteria" value='CRITERIA' />
        <input type="hidden" name="return_url" value="/wisconsin/index" />

        <div class="center">
        <input type="submit" name="change" value="Tenta Novamente a Fase"/>
        <input type="submit" name="change" value="Não quero jogar"/>
        <input type="submit" name="change" value="Desisto"/>
        <input type="submit" name="change" value="Terminei mas acho que está errado"/>
        <input type="submit" name="change" value="Terminei e acho que está certo"/>
        </div>
        <br/>

        <div id="header">&nbsp;</div>
        <div id="main_content">
        <div id="pydiv">
        </div>


    </form>
</body>
</html>