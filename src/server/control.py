#! /usr/bin/env python
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa EICA
# Copyright 2014-2017 Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://j.mp/GNU_GPL3>`__.
#
# EICA é um software livre; você pode redistribuí-lo e/ou
# modificá-lo dentro dos termos da Licença Pública Geral GNU como
# publicada pela Fundação do Software Livre (FSF); na versão 2 da
# Licença.
#
# Este programa é distribuído na esperança de que possa ser útil,
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
# a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
#
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, veja em <http://www.gnu.org/licenses/>

"""Handle http requests.

.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

"""
# import server.modelo as database
import json
import os
from datetime import datetime

from bottle import default_app, route, view, get, post, static_file, request, redirect, run, TEMPLATE_PATH, template

import server.modelo as database

__author__ = 'carlo'
DIR = os.path.dirname(__file__)  # + '/view'
INDEX = os.path.dirname(__file__) + '/../'
LAST = None
PEC = "jogada"
HEAD = "carta casa move tempo ponto value".split()
FAKE = [{k: 10 * i + j for j, k in enumerate(HEAD)} for i in range(4)]
COLOR = dict(game_eica=("fff", 256), torre_londres=("333", 56), wisconsin=("333", 56), jogo_trem=("fff", 256))


@get("/assets/<filepath:re:.*\.(jpg|png|gif|ico|svg)>")
def img(filepath):
    return static_file(filepath, root=u'/home/carlo/Documentos/dev/eica/src/assets')


@get("/api/getlist")
def listing():
    return {"applist": database.DRECORD.getlist()}


@get("/api/getsession")
def session():
    doc_key = request.query.id
    return {"applist": database.DRECORD.get(doc_key)}


def retrieve_data(req):
    jdata = req['data']
    print(jdata)
    return json.loads(jdata)


def retrieve_params(req):
    # print ('retrieve_params', req)
    doc_id = req.pop('doc_id')
    game = req.pop('gamename') if 'gamename' in req else ""
    data = {k: req[k] for k in req}
    print("control:retrieve_params", doc_id, data)
    return {doc_id: data}, game


@route('/')
def hello_world():
    redirect("/static/index.html")


@route('/plot')
def player():
    redirect("/static/eicaplayer.html")


@route('/ei')
def go_eica():
    # redirect('/carinhas/carinhas.html')
    # redirect('/tuple/index.html')
    redirect('/eica/test.html')


@get('/static/img/tol/<filename:re:.*\.(png|jpg|svg|gif)>')
def assets_tol(filename):
    # print('/static/<filename:re:.*\.css>', filename, INDEX + "torre_londres/img/tol/")
    return static_file(filename, root=INDEX + "torre_londres/img/tol/")


@get('/static/assets/<filename:re:.*\.(png|jpg|svg|gif)>')
def assets(filename):
    # print('/static/<filename:re:.*\.css>', filename, INDEX)
    return static_file(filename, root=INDEX + "/assets")


@get('/static/<gamename:re:(game_eica|torre_londres|wisconsin|jogo_trem)>')
@view('register')
def register(gamename):
    # print('/static/register', gamename, 'register.html', DIR + "/views")
    return dict(game=gamename, title=gamename.split("_")[-1].upper(),
                color=COLOR[gamename][0], color_i=COLOR[gamename][1])


@get('/static/wis/<filename:re:.*\.(html|css|png|jpg|svg|gif)>')
def stylecss(filename):
    # print('/static/<filename:re:.*\.css>', filename, INDEX + "wisconsin")
    return static_file(filename, root=INDEX + "wisconsin")


@get('/static/<filename:re:.*\.(html|css)>')
def stylecss(filename):
    # print('/static/<filename:re:.*\.css>', filename, INDEX)
    return static_file(filename, root=DIR + "/views")


@get('/static/<filename:re:.*\.(js)>')
def jscript(filename):
    # print('/static/js/<filename:re:.*\.css>', filename, DIR + "/views")
    return static_file(filename, root=DIR + "/views")


@get('/static/<filename:re:.*\.(png|jpg|gif|svg)>')
def stylecss(filename):
    # print('/static/img/<filename:re:.*\.css>', filename, DIR + "/views")
    return static_file(filename, root=DIR + "/views")


@get('/static/<filename:re:.*\.py>')
def code(filename):
    # print('/static/<filename:re:.*\.css>', filename, INDEX)
    return static_file(filename, root=INDEX)


@get('/static/register')
def register_user():
    global LAST
    jsondata, game = retrieve_params(request.params)
    jsondata = list(jsondata.values())[0]
    jsondata.update({PEC: [], "time": str(datetime.now())})
    gid = database.DRECORD.save(jsondata)
    print('/record/register', gid, jsondata)
    LAST = gid
    return template(game, doc_id=gid)


@get('/record/getid')
def get_user_id_():
    global LAST
    # gid = database.DRECORD.save({PEC: []})
    # print('/record/getid', gid)
    # LAST = gid
    # record = database.DRECORD[LAST]
    # record.update({PEC: []})
    # database.DRECORD[LAST] = record

    return LAST


@get('/pontos')
@view('resultado')
def score():
    try:
        record_id = LAST
        if record_id is None:
            raise Exception()
        record = database.DRECORD.get(record_id)
        # record = record[PEC]
        user = record["user"]
        idade = int(record["idade"][4:]) + 5
        ano = record["ano"][3:]
        sexo = record["sexo"]
        print("score:", dict(user=user, idade=idade, ano=ano, sexo=sexo, result=record["jogada"]))

        return dict(user=user, idade=idade, ano=ano, sexo=sexo, result=record["jogada"])
    except Exception:
        # return dict(user="FAKE", result=FAKE)
        fake = dict(user="FAKE", result=FAKE)
        # print('score', fake)
        return fake


@post('/record/store')
def store():
    try:
        from json import dumps, loads
        jsondata, _ = retrieve_params(request.params)
        record_id = list(jsondata.keys())[0]
        record = database.DRECORD.get(record_id)
        record = loads(record.replace("'", '"')) if isinstance(record, str) else record
        _score = jsondata[record_id]
        _score["tempo"] = str(datetime.now())
        record[PEC] += [_score]
        database.DRECORD.set(record_id, record)
        print('record score:', _score, record_id, record)
        return record
    except Exception as ex:
        print('record score:', ex, record)
        return "Movimento de peça não foi gravado %s" % str(request.params.values())


application = default_app()

if __name__ == "__main__":
    TEMPLATE_PATH.insert(0, INDEX)
    run(host='localhost', port=8080)
