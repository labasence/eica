"""
-*- coding: utf-8 -*-
###############################################
AgileUFRJ - Implementando as teses do PPGI
###############################################
:Author: *Carlo E. T. Oliveira*
:Contact: carlo@nce.ufrj.br
:Date: $Date: 2010/09/07 $
:Status: This is a "work in progress"
:Revision: $Revision: 0.02 $
:Home: `LABASE <http://labase.nce.ufrj.br/>`__
:Copyright: ©2011, `GPL <http://is.gd/3Udt>`__.
"""

# from plugins.api.model import RESULT, API_GAME
# import datetime
from braser.vitollino import Vitollino, Ponto as pt

'''

class Result:
    pass

class ApiGame:
    def retrieve(self, sessionid):
        pass

class RESULT:
    QUIT,CHEAT,CALLOUT,ABORT,FAILURE,TIMEOUT,NORMAL,BELATED,RETRY,ALMOST,SUCCESS = range(-5,6)
NAMES ='TIMEOUT,NORMAL,BELATED,RETRY,ALMOST,SUCCESS,QUIT,CHEAT,CALLOUT,ABORT,FAILURE'.split(',')


API_GAME = ApiGame()

TOLPAGE = "modules/plugins/tol/tol.html"

'''

PUT_BEAD, TAKE_BEAD = 1, 2
HOUSES_DEFAULT = {'h': [], 'b': [1, 2], 'm': [], 'l': [3, 4]}
BEAD_COLOR = ['red', 'nav', 'gre', 'yel']
BEAD_SIEVE = [[[int(bead) for bead in pin] for pin in model.split('_')] for model in
              ',_12_43,4321__,_234_1,3_42_1,41__32,_43_21,2_13_4,_413_2,3_241_,_231_4'.split(',')]
BEAD_MOVES = [int(moves) for moves in '02345566799']  # inclui um 0 na frente pois as fases vão de 1 a 10
BEAD_MODEL = [dict([(550, dict((pin * 60, [BEAD_COLOR[bead - 1] for bead in beads[::-1]])
                               for pin, beads in enumerate(model)))]) for model in BEAD_SIEVE]
MOVE_SIEVE = """n2r2","y2g1y1",["n2y2r2g1r3","n2y2g2r3g1"],["y2g2n3g3y1","n2y1g1n3g3"]
             ,"y1g2y2n2r3n3","y1g2y3n3r2n1","y1g2y2n3y3r2y2"
             ,["n2r2y1r1n3r2y2n2g1","y2n2r3n1y1r2y2n2g1","y2g2n3r3g1y1r2y2n2"]
             ,["n2r2y1r3n1r2g2n2y3","y2n2r3n1y1r2g2y3n2"]"""
PINMAX = (1, 4, 3, 2)

CRITERIA = ['house', 'marker', 'state', 'time']
HEADINGS = ['Pino', 'Conta', 'Estado', 'Tempo']
BUTTONS = "novamente desiste nao_quero fim_errado fim_certo".split()
HAND = ["gre", "nav", "red", "yel"]
html = None


class State:
    def __init__(self):
        self.houses = dict(h=[], b=[0, 1], m=[2], l=[3])
        self.level = 1


class Pin(object):
    def __init__(self, pin, size, model, parent):
        self.beads, self.parent, self.pin, self.model = [], parent, pin, model
        img = "img/tol/pin.png"
        style = {"width": "25px", "height": f"{240 - pin}px", "position": "absolute",
                 "left": f"{model + 2*pin}px", "top": f"{200 + pin}px"}
        # onClick = "this.form.action='{{ size }}';this.form.submit();"
        self.elt = hpin = html.IMG(src=img, style=style)
        if model > 50:
            self.elt.bind("click", self._on_click)
        self.tabuleiro = parent.tabuleiro
        self.movement = self.parent.movement
        parent.tabuleiro <= hpin

    def make_beads(self, beads):
        self.beads = [
            Bead(self.pin, img, size, self.model, self)
            for size, img in enumerate(beads[self.model][self.pin])]

    def _on_click(self, ev):
        self.parent.score(evento=ev, carta=(str(self.pin // 60), str(self.model // 60)), ponto=self.parent.pin(),
                          valor="_PIN_", move=self.parent.movement, _level=3)
        self.parent.movement.as_pin(self)

    def from_pin(self):
        return self.beads.pop()

    def to_pin(self, bead):
        if len(self.beads) >= 4 - self.pin//60:
            return False
        bead.move(self.pin, len(self.beads))
        bead.parent = self
        print("to_pin", self.pin, len(self.beads))
        self.beads.append(bead)
        return True

    def bead(self):
        self.parent.bead()

    def move(self):
        self.parent.movement.as_bead(self.beads)

    def score(self, **kwargs):
        self.parent.score(**kwargs)


class Bead(object):
    def __init__(self, pin, img, size, model, parent):
        self.parent, self.pin, self.model, self.img = parent, pin, model, img
        img = f"img/tol/{img}.png"
        style = {"width": "60px", "height": "60px", "position": "absolute",
                 "left": f"{model+ 2*pin -20}px",
                 "top": f"{380 - 60 * size}px"}
        # onClick = "this.form.action='{{ size }}';this.form.submit();"
        self.elt = hpin = html.IMG(src=img, style=style)
        if model > 50:
            self.elt.bind("click", self._on_click)
        self.tabuleiro = parent.tabuleiro
        self.tabuleiro <= hpin
        parent.tabuleiro <= hpin

    def _on_click(self, ev):
        self.parent.score(evento=ev, carta=(str(self.pin // 60), self.img), ponto=self.parent.bead(),
                          valor="_BEAD_", move=self.parent.movement, _level=3)
        self.parent.move()

    def move(self, pin, position):
        self.elt.style.top = f"{380 - 60 * position}px"
        self.elt.style.left = f"{self.model+ 2*pin -20}px"


class Jogo:
    STATE = State()

    def __init__(self, doc_id, canvas, vitollino):
        outer = self

        class StateToPin:
            def as_pin(self, pin):

                outer.movement = StateFromPin() if pin.to_pin(outer.hand) else outer.movement

            def as_bead(self, bead):
                pin = bead[0].parent
                outer.movement = StateFromPin() if pin.to_pin(outer.hand) else outer.movement

            def __repr__(self):
                return "to_pin"

        class StateFromPin:
            def as_pin(self, pin):
                pass

            def as_bead(self, bead):
                if not bead:
                    return
                bead = bead.pop()
                outer.hand = bead
                bead.move(1, 5)
                outer.movement = StateToPin()

            def __repr__(self):
                return "from_pin"

        self.score = vitollino.score
        self.doc_id, self.round, self._pin, self._bead = doc_id, 0, 0, 0
        self.movement = StateFromPin()
        self.state_from_pin = StateFromPin()
        self.hand = None
        self.tabuleiro = canvas["tabuleiro"]
        # canvas["novamente"].bind("click", self.novamente)
        [canvas[button].bind("click", getattr(self, button)) for button in BUTTONS]
        self.display = canvas["fase"]
        # self._tol = Jogo.STATE
        self.level = 1
        self.score(evento=pt(0, 0), carta="00", ponto=0, valor="torre_londres", move=self.movement, _level=1,
                   doc_id=self.doc_id)
        self.inicia(0)

    def _show(self, level):
        return dict([(50, list(BEAD_MODEL[level].items())[0][1]),
                     list(BEAD_MODEL[level].items())[0],
                     (550, {0: ["red", "nav", "yel", "gre"], 60: [], 120: []})])
        # return dict([(50, dict((pin * 60, [BEAD_COLOR[bead] for bead in beads])
        #                        for pin, beads in enumerate([self._tol.houses[pino]
        #                                                     for pino in 'bml']))),
        #              list(BEAD_MODEL[level].items())[0],
        #              (550, {0: ["red", "nav", "yel", "gre"], 60: [], 120: []})])

    def pin(self):
        self._pin += 1
        return self._pin

    def bead(self):
        self._bead += 1
        return self._bead

    def novamente(self, _):
        self.inicia(0, valor="_REINICIA_")

    def desiste(self, _):
        from browser import window
        self.inicia(0, valor="_DESISTE_")
        window.location = "/"
        pass

    def nao_quero(self, _):
        from browser import window
        self.inicia(0, valor="_NAO_QUERO_")
        window.location = "/"
        pass

    def fim_errado(self, _):
        self.inicia(valor="_FIM_RUIM_")
        pass

    def fim_certo(self, _):
        self.inicia(valor="_FIM_BOM_")
        pass

    def inicia(self, increase=1, valor="_NORMAL_"):
        self._pin, self._bead = 0, 0
        self.hand = None
        self.movement = self.state_from_pin
        self.tabuleiro.html = ""
        self.level = self.level + increase
        self.score(evento=pt(0, 0), carta="00", ponto=self.level, valor=valor, move=self.movement, _level=2,
                   doc_id=self.doc_id)
        self.round += 1
        try:
            beads = self._show(self.level)
        except IndexError:
            from browser import window
            self.score(evento=pt(0, 0), carta="99", ponto=self.level, valor="_FIM_TORRE_",
                       move=self.movement, _level=1, doc_id=self.doc_id)
            window.location = "/"
            return
        self.display.html = "Fase {}".format(self.level)

        print(beads)

        for model in (50, 550):
            if model == 50:
                self.pins = [Pin(pin, size, model, self) for pin, size
                             in ((0, 'big'), (60, 'mid'), (120, 'lit'))]
                _ = [pin.make_beads(beads) for pin in self.pins]
            else:
                pins = [Pin(pin, size, model, self) for pin, size
                        in ((0, 'big'), (60, 'mid'), (120, 'lit'))]
                _ = [pin.make_beads(beads) for pin in pins]

    def register(self):
        print("register")
        self.score(evento=pt(0, 0), carta="00", ponto=0, valor="torre_londres", move=self.movement, _level=1,
                   doc_id=self.doc_id)
        self.inicia(0)
        pass


def main(doc_id):
    global html
    from browser import html as h, document, timer

    html = h
    jogo = Jogo(doc_id, document, Vitollino(name=None))
    # timer.set_timeout(jogo.register(), 1000)


def test():
    global html
    import unittest.mock as mk
    print(BEAD_SIEVE)
    print(BEAD_MODEL)
    print(BEAD_MODEL[0])
    print(list(BEAD_MODEL[3].items()))
    print(list(BEAD_MODEL[6].items())[0])
    html = mk.MagicMock("html")
    html.IMG = mk.MagicMock("IMG")
    doc = mk.MagicMock("doc")
    doc.__le__ = lambda *_: None  # mk.MagicMock("append")

    # Jogo(doc)


if __name__ == '__main__':
    test()
