# -*- coding: utf-8 -*-
"""
###############################################
AgileUFRJ - Implementando as teses do PPGI
###############################################
:Author: *Carlo E. T. Oliveira*
:Contact: carlo@nce.ufrj.br
:Date: $Date: 2010/09/07 $
:Status: This is a "work in progress"
:Revision: $Revision: 0.02 $
:Home: `LABASE <http://labase.nce.ufrj.br/>`__
:Copyright: ©2011, `GPL <http://is.gd/3Udt>`__.
"""

# import wcst
import wisconsin.wcst as wcst
from browser import html, document

# from plugins.api import model as model


WISCPAGE = "wisconsin.html"

CRITERIA = ['carta_resposta', 'categoria', 'acertos', 'cor', 'forma', 'numero', 'outros', 'time']
HEADINGS = ['Carta resposta', 'Categoria', 'Acertos', 'Cor', 'Forma', u'Número', 'Outros', 'Data/Hora']

'''
class WisconsinHandler:
    """
    Joga o Teste de Wisconsin
    """

    def index(self, init="2", level="1"):
        self.title = "Wisconsin"
        houses = {"indice_carta_atual": -1,
                  "categoria": 0,
                  "self.acertos_consecutivos": 0,
                  "self.outros_consecutivos": 0,
                  "wteste": None
                  }

        sessionid = self.get_current_gamesession()
        self._wisc = model.API_GAME().retrieve(sessionid)
        if init == "0":  # nova tentativa
            self._wisc.next(newtrial=True, houses=houses, markers=[], table=[])
        elif init == "1":  # novo nível
            self._wisc.next(newlevel=int(level), houses=houses, markers=[], criteria=CRITERIA, headings=HEADINGS)
        elif init == "2":  # novo jogo
            self._wisc.next(newgame="wisconsin", maxlevel=1, newlevel=int(level), houses=houses, markers=[],
                            criteria=CRITERIA, headings=HEADINGS)

        self.redirect("/wisconsin/play")

    def play(self, result="", **kargs):
        sessionid = 0
        cartasEstimulo = []
        cartaPuxada = None
        if result != "Fim do Jogo":
            self._wisc = model.API_GAME().retrieve(sessionid)
            self._wisc.houses["indice_carta_atual"] = self._wisc.houses["indice_carta_atual"] + 1
            indice_carta_atual = self._wisc.houses["indice_carta_atual"]
            self._wisc.next(houses=self._wisc.houses)

            cartasEstimulo = wcst.listaCartasEstimulo
            cartaPuxada = wcst.listaCartasResposta[indice_carta_atual]

        self.render(WISCPAGE, CRITERIA=CRITERIA, \
                    CARTAPUXADA=cartaPuxada, CARTASESTIMULO=cartasEstimulo, \
                    MSG=result, \
                    SESSIONID=sessionid, RESULT=model.RESULT)

    def click(self, opcao, **kargs):
        opcao = int(opcao)
        sessionid = self.get_current_gamesession()

        self._wisc = model.API_GAME().retrieve(sessionid)
        indice_carta_atual = self._wisc.houses["indice_carta_atual"]
        categoria = self._wisc.houses["categoria"]
        self.acertos_consecutivos = self._wisc.houses["self.acertos_consecutivos"]
        self.outros_consecutivos = self._wisc.houses["self.outros_consecutivos"]

        indice_carta = (indice_carta_atual % wcst.numCartasResposta) + 1
        cartaResposta = wcst.listaCartasResposta[indice_carta_atual]
        cartaEstimulo = wcst.listaCartasEstimulo[opcao]

        tudoDiferente = cartaResposta.testaTudoDiferente(cartaEstimulo)

        if cartaResposta.testaMesmaCategoria(cartaEstimulo,
                                             wcst.listaCategorias[categoria]):
            self.acertos_consecutivos += 1
            resultado_teste = "Certo"
        else:
            self.acertos_consecutivos = 0
            resultado_teste = "Errado"

        if tudoDiferente:
            self.outros_consecutivos += 1
        else:
            self.outros_consecutivos = 0

        if self.outros_consecutivos == 3:
            self.outros_consecutivos = 0
            resultado_teste = u"%s.<br/>Leia atentamente as instruções: %s" % (resultado_teste, wcst.instrucoes_teste())

        # Grava a jogada no banco de dados
        table = [dict(
            carta_resposta=indice_carta,
            categoria=wcst.listaCategorias[categoria],
            acertos=self.acertos_consecutivos,
            cor=cartaResposta.testaMesmaCategoria(cartaEstimulo,
                                                  wcst.listaCategorias[0]),
            forma=cartaResposta.testaMesmaCategoria(cartaEstimulo,
                                                    wcst.listaCategorias[1]),
            numero=cartaResposta.testaMesmaCategoria(cartaEstimulo,
                                                     wcst.listaCategorias[2]),
            outros=tudoDiferente,
            time=str(datetime.datetime()))]

        # Se os acertos consecutivos chegarem a 10, troca a categoria
        if self.acertos_consecutivos == 10:
            self.acertos_consecutivos = 0
            categoria += 1

        houses = {"indice_carta_atual": indice_carta_atual,
                  "categoria": categoria,
                  "self.acertos_consecutivos": self.acertos_consecutivos,
                  "self.outros_consecutivos": self.outros_consecutivos,
                  "wteste": None
                  }
        self._wisc.next(houses=houses, table=table)

        # Termina o teste se esgotar as categorias ou fim das cartas respostas.
        if ((categoria >= len(wcst.listaCategorias)) or
                (indice_carta_atual >= len(wcst.listaCartasResposta) - 1)):
            resultado_teste = "Fim do Jogo"

        params = {'username': 'administrator', 'password': 'xyz'}
        self.redirect('/wisconsin/play?' + urllib.parse.urlencode(params))

    def render(self, WISCPAGE, CRITERIA, CARTAPUXADA, CARTASESTIMULO, MSG, SESSIONID, RESULT):
        pass

    def redirect(self, param):
        pass
'''
ICON = dict(CERTO="ok.png", ERRADO="cancel.png")
MSG_CLASS = dict(CERTO=("CERTO", "flash_certo"), ERRADO=("ERRADO", "flash_errado"))


class Carta:
    def __init__(self, carta, ul, indice, jogo):
        self.jogo = jogo
        self.cria(carta, ul, indice)

    def cria(self, carta, ul, indice):
        img = f"wis/img/{carta.img}"
        alt = f"{carta.pegaAtributosCarta()}"
        title = f"{carta.pegaAtributosCarta()}"
        li = html.LI()
        ul <= li
        hpin = html.IMG(src=img, alt=alt, title=title)
        # hpin.bind("click", self.onclick)
        div = html.DIV(Id=f"a_carta_{indice}", Class=f"carta {carta.color}", tabindex="0")
        div <= hpin
        li <= div
        div.bind("click", self.onclick)
        for item in range(carta.num - 1):
            img = f"wis/img/{carta.img}"
            hpin = html.IMG(src=img)
            hpin.bind("click", self.onclick)
            div.bind("click", self.onclick)
            div <= hpin

    def onclick(self, ev):
        carta = ev.target.id
        self.jogo.escolhe_carta("flash_certo", carta)


class Jogo:
    def __init__(self, doc_id):
        self.indice_carta = 0
        self.indice_carta_atual = 0
        self.doc_id = doc_id
        self.tabuleiro = document["main_content"]
        self.msg = "ERRADO"
        self.cartapuxada = wcst.listaCartasResposta.pop(0)
        self.cartasestimulo = wcst.listaCartasEstimulo
        self.categoria = 0
        self.acertos_consecutivos = 0
        self.outros_consecutivos = 0

        if self.msg:
            self.msg_div = html.DIV(self.msg, id="status_block", Class="flash_errado")
            self.tabuleiro <= self.msg_div

        if self.cartapuxada:
            self.divout = html.DIV()
            self.tabuleiro <= self.divout
            self.puxa_carta(self.divout)

            self.tabuleiro <= html.P("Combina com qual das cartas abaixo?")
            ul = html.UL()
            self.tabuleiro <= ul
            for (indice, carta) in enumerate(self.cartasestimulo):
                Carta(carta, ul, indice, self)
            self.tabuleiro <= html.P("&nbsp;" * 30)

    def puxa_carta(self, div_carta):
        div_carta.html = ""
        divout = html.DIV(Class=f"carta puxada {self.cartapuxada.color}")
        div_carta <= divout
        img = f"wis/img/{self.cartapuxada.img}"
        alt = f"{self.cartapuxada.pegaAtributosCarta()}"
        title = f"{self.cartapuxada.pegaAtributosCarta()}"
        hpin = html.IMG(src=img, alt=alt, title=title)
        divout <= hpin
        for item in range(self.cartapuxada.num - 1):
            img = f"wis/img/{self.cartapuxada.img}"
            hpin = html.IMG(src=img)
            divout <= hpin

    def mostra_resultado(self):
        self.cartapuxada = wcst.listaCartasResposta.pop(0)
        self.puxa_carta(self.divout)

    def escolhe_carta(self, resultado, carta):
        self.cartapuxada = wcst.listaCartasResposta.pop(0)
        self.puxa_carta(self.divout)
        
        # self.msg_div.html = self.testa_carta(carta)
        self.msg_div.html, self.msg_div.className = MSG_CLASS[self.testa_carta(carta)]

    def testa_carta(self, opcao):
        opcao = int(opcao[-1])
        '''
        indice_carta_atual = self._wisc.houses["indice_carta_atual"]
        categoria = self._wisc.houses["categoria"]
        self.acertos_consecutivos = self._wisc.houses["self.acertos_consecutivos"]
        self.outros_consecutivos = self._wisc.houses["self.outros_consecutivos"]
        indice_carta = (indice_carta_atual % wcst.numCartasResposta) + 1
        '''
        cartaResposta = self.cartapuxada  # wcst.listaCartasResposta[indice_carta_atual]
        cartaEstimulo = wcst.listaCartasEstimulo[opcao]

        tudoDiferente = cartaResposta.testaTudoDiferente(cartaEstimulo)

        if cartaResposta.testaMesmaCategoria(cartaEstimulo,
                                             wcst.listaCategorias[self.categoria]):
            self.acertos_consecutivos += 1
            resultado_teste = "CERTO"
        else:
            self.acertos_consecutivos = 0
            resultado_teste = "ERRADO"

        if tudoDiferente:
            self.outros_consecutivos += 1
        else:
            self.outros_consecutivos = 0

        if self.outros_consecutivos == 3:
            self.outros_consecutivos = 0
        print("opcao, resposta, estimulo, tdiferente, outros", opcao, cartaResposta, cartaEstimulo, tudoDiferente, self.outros_consecutivos, resultado_teste)
        return resultado_teste

        # Grava a jogada no banco de dados
        table = [dict(
            carta_resposta=self.indice_carta,
            categoria=wcst.listaCategorias[self.categoria],
            acertos=self.acertos_consecutivos,
            cor=cartaResposta.testaMesmaCategoria(cartaEstimulo,
                                                  wcst.listaCategorias[0]),
            forma=cartaResposta.testaMesmaCategoria(cartaEstimulo,
                                                    wcst.listaCategorias[1]),
            numero=cartaResposta.testaMesmaCategoria(cartaEstimulo,
                                                     wcst.listaCategorias[2]),
            outros=tudoDiferente)]

        # Se os acertos consecutivos chegarem a 10, troca a categoria
        if self.acertos_consecutivos == 10:
            self.acertos_consecutivos = 0
            self.categoria += 1

        houses = {"indice_carta_atual": self.indice_carta_atual,
                  "categoria": self.categoria,
                  "self.acertos_consecutivos": self.acertos_consecutivos,
                  "self.outros_consecutivos": self.outros_consecutivos,
                  "wteste": None
                  }
        # self._wisc.next(houses=houses, table=table)

        # Termina o teste se esgotar as categorias ou fim das cartas respostas.
        if ((self.categoria >= len(wcst.listaCategorias)) or
                (self.indice_carta_atual >= len(wcst.listaCartasResposta) - 1)):
            self.termina_jogo()

        # self.redirect('/wisconsin/play?' + urllib.parse.urlencode(params))
        return resultado_teste

    def termina_jogo(self):
        pass


def main(doc_id):
    Jogo(doc_id)
