# -*- coding: utf-8 -*-
"""
###############################################
AgileUFRJ - Implementando as teses do PPGI
###############################################
:Author: *Carlo E. T. Oliveira*
:Contact: carlo@nce.ufrj.br
:Date: $Date: 2010/09/07 $
:Status: This is a "work in progress"
:Revision: $Revision: 0.02 $
:Home: `LABASE <http://labase.nce.ufrj.br/>`__
:Copyright: ©2011, `GPL <http://is.gd/3Udt>`__.
"""


from plugins.api import model as model

TRAINZPAGE = "modules/plugins/trainz/trainz.html"

CRITERIA = ['marker', 'house', 'state', 'score', 'time']
HEADINGS = ['Marcador', 'Casa', 'Movimento', 'Pontos', 'Tempo']


class TrainzHandler:
    """
    Manobrando o Trem no Desvio
    """

    @libs.methoddispatcher.authenticated
    def index(self, init="1"):
        self.title = "Manobrando o Trem"
        sessionid = 0
        if init == "1":
            self._trainz = model.API_GAME().retrieve(sessionid)
            self._trainz.next(newgame="trainz", newlevel=1, criteria=CRITERIA, headings=HEADINGS)

        self.render(TRAINZPAGE, TITLE=self.title, BEADS={}, HAND=[], CRITERIA=CRITERIA, \
                    SESSIONID=sessionid, RESULT=model.RESULT)

    def render(self, TRAINZPAGE, TITLE, BEADS, HAND, CRITERIA, SESSIONID, RESULT):
        pass

